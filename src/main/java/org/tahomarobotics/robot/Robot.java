/**
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.robot;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tahomarobotics.robot.chassis.Chassis;

import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.command.Scheduler;

public class Robot extends TimedRobot {

	private static final Logger LOGGER = LoggerFactory.getLogger(Robot.class);
	
	// collection of sub-systems added to the robot
	private final List<UpdateIF> subsystems = new ArrayList<>();

	/**
	 * robotInit is called once on start of the robot code.  Create all 
	 * sub-systems and add to collection
	 */
	@Override
	public void robotInit() {
		LOGGER.info("robotInit()");
		subsystems.add(OI.getInstance());
		subsystems.add(Chassis.getInstance());
	}

	/**
	 * robotPeriodic is called periodically every 20 milliseconds unless the 
	 * period is set to something different via <code>setPeriod</code>
	 */
	@Override
	public void robotPeriodic() {
		
		// run scheduler in every mode
		Scheduler.getInstance().run();
		
		// update each sub-system
		for (UpdateIF subsystem : subsystems) {
			subsystem.update();
		}
	}

	@Override
	public void disabledInit() {
		LOGGER.info("disabledInit()");
	}

	@Override
	public void autonomousInit() {
		LOGGER.info("autonomousInit()");
	}

	@Override
	public void teleopInit() {
		LOGGER.info("teleopInit()");
	}

	@Override
	public void disabledPeriodic() {
	}

	@Override
	public void autonomousPeriodic() {
	}

	@Override
	public void teleopPeriodic() {
	}

}
