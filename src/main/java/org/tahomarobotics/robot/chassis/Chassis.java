/**
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.robot.chassis;

import org.tahomarobotics.robot.RobotMap;
import org.tahomarobotics.robot.UpdateIF;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;
import com.ctre.phoenix.motorcontrol.can.VictorSPX;

import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.command.Subsystem;

/**
 * Chassis class controls the drive base motors, transmission and provides velocity feedback.
 *
 */
public class Chassis extends Subsystem implements UpdateIF {

	// Gear state enumeration with solenoid values
	public enum Gear {
		LOW(false),
		HIGH(true);

		// used to command solenoid
		public final boolean value;

		Gear(boolean value){
			this.value = value;
		}
	}

	// singleton instance
	private static final Chassis INSTANCE = new Chassis();
	
	// motor instances
	private final TalonSRX leftMaster = new TalonSRX(RobotMap.LEFT_REAR_MOTOR);
	private final VictorSPX leftSlave1 = new VictorSPX(RobotMap.LEFT_MIDDLE_MOTOR);
	private final VictorSPX leftSlave2 = new VictorSPX(RobotMap.LEFT_FRONT_MOTOR);
	private final TalonSRX rightMaster = new TalonSRX(RobotMap.RIGHT_REAR_MOTOR);
	private final VictorSPX rightSlave1 = new VictorSPX(RobotMap.RIGHT_MIDDLE_MOTOR);
	private final VictorSPX rightSlave2 = new VictorSPX(RobotMap.RIGHT_FRONT_MOTOR);

	// gear solenoid 
	private final Solenoid gearShifter = new Solenoid(RobotMap.PCM_MODULE, RobotMap.DRIVE_SOLENOID);
	
	// holds the state of the transmission shifter
	private Gear gearState = Gear.LOW;

	/**
	 * Constructor is used to initialize motor controller for proper operation
	 */
	private Chassis() {
		
		// invert the left motor controllers
		leftMaster.setInverted(true);
		leftSlave1.setInverted(true);
		leftSlave2.setInverted(true);

		// slave the controller to the masters
		leftSlave1.follow(leftMaster);
		leftSlave2.follow(leftMaster);
		rightSlave1.follow(rightMaster);
		rightSlave2.follow(rightMaster);
	}
	
	/**
	 * @return the Chassis singleton instance
	 */
	public static Chassis getInstance() {
		return INSTANCE;
	}
	
	/**
	 * Install telop-drive command.  This command runs until
	 * another chassis command requires the Chassis sub-system.
	 */
	@Override
	protected void initDefaultCommand() {
		setDefaultCommand(new TeleopDriveCommand());
	}
	
	/**
	 * update any needed state or observers
	 */
	@Override
	public void update() {
	}

	/**
	 * Command the transmission shifter to transition
	 * @param gear - HIGH or LOW
	 */
	public void setGear(Gear gear) {
		gearShifter.set(gear.value);
		gearState  = gear;
	}
	
	/**
	 * Returns the current state of the transmission gear
	 * @return HIGH or LOW
	 */
	public Gear getGearState() {
		return gearState;
	}

	/**
	 * Set the power level of the drive base motors
	 * 
	 * @param forwardPower - forward power level
	 * @param rotatePower - rotate power level
	 */
	public void setPower(double forwardPower, double rotatePower) {
		setLeftRightPower(forwardPower - rotatePower, forwardPower + rotatePower);
	}
	
	/**
	 * Set the power level of the drive base motors
	 * 
	 * @param left - left power level
	 * @param right - right power level
	 */
	public void setLeftRightPower(double left, double right) {				
		leftMaster.set(ControlMode.PercentOutput, left);
		rightMaster.set(ControlMode.PercentOutput, right);
	}
}
