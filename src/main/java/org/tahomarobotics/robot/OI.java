/**
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.robot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tahomarobotics.robot.chassis.Chassis.Gear;
import org.tahomarobotics.robot.chassis.ShiftCommand;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.GenericHID.Hand;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.buttons.JoystickButton;
import edu.wpi.first.wpilibj.hal.HAL;

/**
 * Operator Interface defines the driver and manipulator controls for the Robot.
 * It maps controllers and controller buttons and sticks to functions.
 */
@SuppressWarnings("unused")
public class OI implements UpdateIF {

	private static final Logger LOGGER = LoggerFactory.getLogger(OI.class);
	
	// The amount of the analog control range removed when the value are near zero
	private static final double DEAD_BAND = 9.0 / 127.0;
	
	private static final OI instance = new OI();
	
	// Controller for the Driver Functions
	private final XboxController driverController = new XboxController(0);
	
	private final JoystickButton driverButtonRBumber;
	private final JoystickButton driverButtonLBumber;
	private final JoystickButton driverButtonA;
	private final JoystickButton driverButtonB;	
	private final JoystickButton driverButtonX;
	private final JoystickButton driverButtonY;
	
	// If the controller is plugged in during Robot start this will be true
	private final boolean connected = driverController.getButtonCount() > 0;
		
	private OI() {
		
		// only start polling the buttons if connected, otherwise is will WARN over and over
		if (connected) {
			LOGGER.info("Controller connected");
			driverButtonRBumber = new JoystickButton(driverController, 6);
			driverButtonLBumber = new JoystickButton(driverController, 5);
			driverButtonA = new JoystickButton(driverController, 1);
			driverButtonB = new JoystickButton(driverController, 2);
			driverButtonX = new JoystickButton(driverController, 3);
			driverButtonY = new JoystickButton(driverController, 4);
			
			driverButtonRBumber.whenPressed(new ShiftCommand(Gear.LOW));
			driverButtonLBumber.whenPressed(new ShiftCommand(Gear.HIGH));			

			
		} else {
			LOGGER.warn("Controller not connected");
			driverButtonRBumber = null;
			driverButtonLBumber = null;
			driverButtonA = null;
			driverButtonB = null;
			driverButtonX = null;
			driverButtonY = null;
		}		
	}
	
	public static final OI getInstance() {
		return instance;
	}
	
	@Override
	public void update() {
	}

	/**
	 * Removes small values emitted from the controller axis when stick is relaxed.
	 * 
	 * Returns the value only if value isn't in the range of -DEAD_BAND to +DEAD_BAND
	 */
	private double applyDeadBand(double value){
		return applyDeadBand(value, DEAD_BAND);
	}

	/**
	 * Remove a small dead-band.  After remove the dead-band value, the result is scaled
	 * to ensure full range can be produced.
	 * 
	 * @param value - raw analog input
	 * @param deadBand - small value to remove
	 * 
	 * @return processed analog value
	 */
	private double applyDeadBand(double value, double deadBand) {
		double absValue = Math.abs(value);
		return (absValue < deadBand) ? 0 : ((absValue - deadBand) / (1.0 - deadBand) * Math.signum(value));
	}

	/**
	 * Returns percent of left throttle.
	 * 
	 * @return left throttle (0% to 100%)
	 */
	public double getLeftThrottle() {
		return connected ? -applyDeadBand(driverController.getY(Hand.kLeft)) : 0;
	}

	/**
	 * Returns percent of right throttle.
	 * 
	 * @return right throttle (0% to 100%)
	 */
	public double getRightThrottle() {
		return connected ? -applyDeadBand(driverController.getY(Hand.kRight)) : 0;
	}
}
