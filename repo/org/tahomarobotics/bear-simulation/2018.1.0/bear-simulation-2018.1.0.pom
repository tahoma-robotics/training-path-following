<!-- 
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE. 
-->
<project xmlns="http://maven.apache.org/POM/4.0.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">

	<modelVersion>4.0.0</modelVersion>

	<groupId>org.tahomarobotics</groupId>
	<artifactId>bear-simulation</artifactId>
	<version>2018.1.0</version>

	<name>Bear Robot Simulation</name>
	<description>robotics simulation for FRC robotics java development</description>

	<url>https://gitlab.com/tahoma-robotics/bear-simulation</url>
	
	<scm>
		<url>https://gitlab.com/tahoma-robotics/bear-simulation.git</url>
	</scm>
	
	<organization>
		<url>http://tahomarobotics.org/</url>
		<name>Bear Metal Robotics - FRC 2046</name>
	</organization>
	
	<inceptionYear>2018</inceptionYear>

	<licenses>
		<license>
			<name>The MIT License (MIT)</name>
			<url>https://opensource.org/licenses/MIT</url>
		</license>
	</licenses>

	<developers>
		<developer>
			<name>Bear Metal Programming Team</name>
			<email>developers@tahomarobotics.org</email>
			<organization>Bear Metal Programming Team</organization>
			<organizationUrl>http://tahomarobotics.org/</organizationUrl>
			<timezone>UTC+08:00</timezone>
		</developer>
	</developers>

	<properties>
		<!-- remove warning about platform dependent -->
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
	</properties>

	<repositories>
		<repository>
			<id>first-robotics</id>
			<name>FIRST Robotics release repository</name>
			<url>http://first.wpi.edu/FRC/roborio/maven/release/</url>
		</repository>
		<repository>
			<id>ctr-electronics</id>
			<name>CTRE release repository</name>
			<url>http://devsite.ctr-electronics.com/maven/release</url>
		</repository>
	</repositories>

	<dependencies>
 
        <!-- CTRE Libraries -->
		<dependency>
			<groupId>com.ctre.phoenix</groupId>
			<artifactId>api-java</artifactId>
			<version>5.9.2</version>
		</dependency>

		<dependency>
			<groupId>edu.wpi.first.wpilibj</groupId>
			<artifactId>wpilibj-java</artifactId>
			<version>2018.4.1</version>
		</dependency>
		<dependency>
			<groupId>edu.wpi.first.ntcore</groupId>
			<artifactId>ntcore-java</artifactId>
			<version>4.0.0</version>
			<!-- corrupt artifact <version>4.1.0</version> -->
		</dependency>
		
		<dependency>
			<groupId>org.javassist</groupId>
			<artifactId>javassist</artifactId>
			<version>3.22.0-GA</version>
		</dependency>
		
		<dependency>
    		<groupId>org.lwjgl.osgi</groupId>
    		<artifactId>org.lwjgl.glfw</artifactId>
    		<version>3.1.6</version>
		</dependency>


		<!-- https://mvnrepository.com/artifact/org.apache.logging.log4j/log4j-slf4j-impl -->
		<dependency>
    		<groupId>org.apache.logging.log4j</groupId>
    		<artifactId>log4j-slf4j-impl</artifactId>
    		<version>2.11.0</version>
		</dependency>
		
	</dependencies>

	<build>
		<plugins>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>3.7.0</version>
				<configuration>
					<source>1.8</source>
					<target>1.8</target>
					<fork>true</fork>
				</configuration>
			</plugin>
			
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-jar-plugin</artifactId>
				<version>3.1.0</version>
				<configuration>
					<archive>
						<index>true</index>
						
						<manifestEntries>
							<Premain-Class>org.tahomarobotics.sim.lib.agent.Agent</Premain-Class>
                			<Can-Redefine-Classes>true</Can-Redefine-Classes>
                			<Can-Retransform-Classes>true</Can-Retransform-Classes>
                			<Can-Set-Native-Method-Prefix>true</Can-Set-Native-Method-Prefix>
                			<Implementation-Title>RobotSim</Implementation-Title>
						</manifestEntries>

					</archive>
				</configuration>
			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-source-plugin</artifactId>
				<version>3.0.1</version>
				<executions>
					<execution>
						<id>attach-sources</id>
						<goals>
							<goal>jar</goal>
						</goals>
					</execution>
				</executions>
			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-javadoc-plugin</artifactId>
				<version>3.0.1</version>
				<executions>
					<execution>
						<id>attach-javadoc</id>
						<goals>
							<goal>jar</goal>
						</goals>
						<configuration>
							<encoding>UTF-8</encoding>
						</configuration>
					</execution>
				</executions>
			</plugin>
			
			<plugin>
				<artifactId>maven-deploy-plugin</artifactId>
				<version>2.8.2</version>
				<executions>
					<execution>
						<id>default-deploy</id>
						<phase>deploy</phase>
						<goals>
							<goal>deploy</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
			
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-release-plugin</artifactId>
				<version>2.5.3</version>
				<configuration>
					<localCheckout>true</localCheckout>
					<pushChanges>false</pushChanges>
					<mavenExecutorId>forked-path</mavenExecutorId>
					<arguments>-Dgpg.passphrase=${gpg.passphrase}</arguments>
				</configuration>
				<dependencies>
					<dependency>
						<groupId>org.apache.maven.scm</groupId>
						<artifactId>maven-scm-provider-gitexe</artifactId>
						<version>1.9.5</version>
					</dependency>
				</dependencies>
			</plugin>

			<plugin>
				<groupId>org.sonatype.plugins</groupId>
				<artifactId>nexus-staging-maven-plugin</artifactId>
				<version>1.6.7</version>
				<extensions>true</extensions>
				<configuration>
					<serverId>ossrh</serverId>
					<nexusUrl>https://oss.sonatype.org/</nexusUrl>
					<autoReleaseAfterClose>true</autoReleaseAfterClose>
				</configuration>
			</plugin>

		</plugins>
	</build>
	
	<distributionManagement>
		<snapshotRepository>
			<id>ossrh</id>
			<url>https://oss.sonatype.org/content/repositories/snapshots</url>
		</snapshotRepository>
		<repository>
			<id>ossrh</id>
			<url>https://oss.sonatype.org/service/local/staging/deploy/maven2/</url>
		</repository>
	</distributionManagement>
	
	<profiles>
		<!-- GPG Signature on release -->
		<profile>
			<id>release-sign-artifacts</id>
			<activation>
				<property>
					<name>performRelease</name>
					<value>true</value>
				</property>
			</activation>
			<build>
				<plugins>
					<plugin>
						<groupId>org.apache.maven.plugins</groupId>
						<artifactId>maven-gpg-plugin</artifactId>
						<version>1.6</version>
						<executions>
							<execution>
								<id>sign-artifacts</id>
								<phase>verify</phase>
								<goals>
									<goal>sign</goal>
								</goals>
							</execution>
						</executions>
					</plugin>
				</plugins>
			</build>
		</profile>
	</profiles>

</project>